package by.epam.pahoda.entities;

public class ClientAccount {
    private Integer id;
    private String login;
    private String password;
    private String name;
    private String surname;
    private Float rating;
    private Integer discount;

    public ClientAccount() {
    }

    public ClientAccount(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public ClientAccount(Integer id, String login, String name,
     String surname, Float rating, Integer discount) {
        this.id = id;
        this.login = login;
        this.name = name;
        this.surname = surname;
        this.rating = rating;
        this.discount = discount;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }
}
