package by.epam.pahoda.entities;

public class Picture {
    private Integer id;
    private String link;
    private String description;
    private Float rating;
    private String author;

    public Picture(){}

    public Picture(Integer id, String link, String description, String author){
        this.id = id;
        this.link = link;
        this.description = description;
        this.author = author;
    }

    public Picture(Integer id, String link, String description, Float rating, String author){
        this.id = id;
        this.link = link;
        this.description = description;
        this.rating = rating;
        this.author = author;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}
