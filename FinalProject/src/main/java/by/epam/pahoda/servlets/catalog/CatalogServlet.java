package by.epam.pahoda.servlets.catalog;

import by.epam.pahoda.entities.Picture;
import by.epam.pahoda.utils.Database;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/catalog")
public class CatalogServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("image/png");
        Database temp = new Database();
        List<Picture> pictures = temp.catalogTable();
        req.setAttribute("pictures", pictures);
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/pages/catalog-page.jsp");
        requestDispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
