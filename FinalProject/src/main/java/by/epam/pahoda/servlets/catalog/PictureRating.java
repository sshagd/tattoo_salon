package by.epam.pahoda.servlets.catalog;

import by.epam.pahoda.entities.ClientAccount;
import by.epam.pahoda.utils.Database;
import by.epam.pahoda.utils.Logger;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;

@WebServlet("/pic-rate")
public class PictureRating extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Integer value = Integer.valueOf(req.getParameter("rating"));
        Integer pictureID = value / 10;
        Float newValue = (float)value % 10;
        Database temp = new Database();
        Float currentRating = temp.pictureRating(pictureID);
        Float newrating = (currentRating + newValue) / 2;
        DecimalFormat df = new DecimalFormat("0.00");
        String format = df.format(newrating);
        Float newRating = null;
        try {
            newRating = df.parse(format).floatValue();
        } catch (ParseException e) {
            Logger.logger.severe(e.getMessage());
        }
        temp.ratingUpdate(pictureID, newRating);
        resp.sendRedirect("/Tariffs_war_exploded/catalog");
        HttpSession session = req.getSession();
        ClientAccount client = (ClientAccount) session.getAttribute("user");
        if(client != null) {
            Float clientRating = client.getRating();
            Integer clientDiscount = client.getDiscount();
            if(clientRating < 10){
                temp.userRatingUpdate(client.getLogin(), (float)(clientRating+0.5), (clientDiscount + 2));
                client = temp.userInfo(client.getLogin());
                session.setAttribute("user", client);
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        doGet(req, resp);
    }
}
