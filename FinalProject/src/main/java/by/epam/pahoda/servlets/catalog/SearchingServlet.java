package by.epam.pahoda.servlets.catalog;

import by.epam.pahoda.entities.Picture;
import by.epam.pahoda.utils.Searching;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/search")
public class SearchingServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        String str = req.getParameter("param");
        Searching temp = new Searching();
        List<Picture> pictures = temp.search(str);
        req.setAttribute("pictures", pictures);
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/pages/catalog-page.jsp");
        requestDispatcher.forward(req, resp);
    }
}
