package by.epam.pahoda.servlets.order;

import by.epam.pahoda.entities.ClientAccount;
import by.epam.pahoda.utils.OrderPicture;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/pic-add")
public class AddPicture extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        String link = request.getParameter("link");
        String description = request.getParameter("description");
        HttpSession session = request.getSession();
        ClientAccount user = (ClientAccount) session.getAttribute("user");
        String author = user.getLogin();
        OrderPicture order = new OrderPicture();
        if(order.test(link) != null){
            RequestDispatcher requestDispatcher = request.getRequestDispatcher("/pages/error_pages/picAddError.jsp");
            requestDispatcher.forward(request, response);
        } else {
            order.pictureOrder(link, description, author);
            RequestDispatcher requestDispatcher = request.getRequestDispatcher("/pages/user.jsp");
            requestDispatcher.forward(request, response);
        }
    }
}
