package by.epam.pahoda.servlets.order;

import by.epam.pahoda.utils.OrderPicture;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/response")
public class OrderResponse extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        if(req.getParameter("yes") != null) {
            Integer id = Integer.valueOf(req.getParameter("yes"));
            OrderPicture newPic = new OrderPicture();
            newPic.picAdd(id);
            resp.sendRedirect("/Salon_war_exploded/orders");
        } else {
            Integer id = Integer.valueOf(req.getParameter("no"));
            OrderPicture newPic = new OrderPicture();
            newPic.picRemove(id);
            resp.sendRedirect("/Salon_war_exploded/orders");
        }
    }
}
