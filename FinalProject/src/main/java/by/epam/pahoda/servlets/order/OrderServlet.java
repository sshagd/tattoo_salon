package by.epam.pahoda.servlets.order;

import by.epam.pahoda.entities.Picture;
import by.epam.pahoda.utils.OrderPicture;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/orders")
public class OrderServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        OrderPicture temp = new OrderPicture();
        List<Picture> orders = temp.ordersTable();
        req.setAttribute("orders", orders);
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/pages/orders.jsp");
        requestDispatcher.forward(req, resp);
    }
}
