package by.epam.pahoda.servlets.user_action;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

@WebServlet({ "/", "/index" })
public class HomeServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    public HomeServlet() { super(); }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        if(session.getAttribute("bundle") == null){
            Locale defaultLocale = new Locale("be", "BY");
            ResourceBundle bundle = ResourceBundle.getBundle("text_be_BY", defaultLocale);
            request.setAttribute("bundle", bundle.getBaseBundleName());
        }
        request.setCharacterEncoding("UTF-8");
        if(session.getAttribute("user") != null){
            RequestDispatcher dispatcher = request.getRequestDispatcher("/pages/user.jsp");
            dispatcher.forward(request, response);
        } else if(session.getAttribute("admin") != null){
            response.sendRedirect("/Salon_war_exploded/user");
        } else {
            RequestDispatcher dispatcher = request.getRequestDispatcher("/index.jsp");
            dispatcher.forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
