package by.epam.pahoda.servlets.user_action;

import by.epam.pahoda.entities.ClientAccount;
import by.epam.pahoda.utils.Database;
import org.apache.commons.codec.digest.DigestUtils;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.List;

public class UserLogIn extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        if(session.getAttribute("admin") != null){
            request.setAttribute("clients", action());
            RequestDispatcher requestDispatcher = request.getRequestDispatcher("/pages/adminProfile.jsp");
            requestDispatcher.forward(request, response);
        } else {
            RequestDispatcher requestDispatcher = request.getRequestDispatcher("/pages/user.jsp");
            requestDispatcher.forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        String passwordHash = DigestUtils.sha256Hex(password);
        ClientAccount client = new ClientAccount(login, passwordHash);

        if(client.getLogin().equals("admin")){
            session.setAttribute("admin", "admin");
            request.setAttribute("clients", action());
            RequestDispatcher requestDispatcher = request.getRequestDispatcher("/pages/adminProfile.jsp");
            requestDispatcher.forward(request, response);
        } else {
            Database temp = new Database();
            if(temp.findUser(client.getLogin(), client.getPassword())){
                ClientAccount currentClient = temp.userInfo(login);
                session.setAttribute("user", currentClient);
                RequestDispatcher requestDispatcher = request.getRequestDispatcher("/pages/user.jsp");
                requestDispatcher.forward(request, response);
            } else {
                RequestDispatcher requestDispatcher = request.getRequestDispatcher("/pages/error_pages/auth-error.jsp");
                requestDispatcher.forward(request, response);
            }
        }
    }

    List<ClientAccount> action(){
        Database temp = new Database();
        return temp.clientTable();
    }
}
