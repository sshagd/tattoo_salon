package by.epam.pahoda.servlets.user_action;

import by.epam.pahoda.entities.ClientAccount;
import by.epam.pahoda.utils.Database;
import org.apache.commons.codec.digest.DigestUtils;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class UserRegistration extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        String login = request.getParameter("login");
        String password = request.getParameter("pass");
        String passwordHash = DigestUtils.sha256Hex(password);
        String name = request.getParameter("name");
        String surname = request.getParameter("surname");
        Database temp = new Database();
        if(temp.userInfo(login) != null){
            RequestDispatcher requestDispatcher = request.getRequestDispatcher("/pages/error_pages/regError.jsp");
            requestDispatcher.forward(request, response);
        } else {
            temp.clientAdd(login, passwordHash, name, surname);
            ClientAccount currentClient = temp.userInfo(login);
            HttpSession session = request.getSession();
            session.setAttribute("user", currentClient);
            RequestDispatcher requestDispatcher = request.getRequestDispatcher("/pages/user.jsp");
            requestDispatcher.forward(request, response);
        }
    }
}
