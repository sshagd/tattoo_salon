package by.epam.pahoda.utils;

import by.epam.pahoda.entities.ClientAccount;
import by.epam.pahoda.entities.Picture;
import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class Database {

    public Database() {
    }

    static Statement statement;

    static {
            initClients();
    }

    static void initClients(){
        String userName = "root";
        String pass = "password";
        String connectionUrl = "jdbc:mysql://localhost:3306/tatoo_salon";

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            Logger.logger.severe(e.getMessage());
        }
        try{
            Connection connection = DriverManager.getConnection(connectionUrl, userName, pass);
            statement = connection.createStatement();
        } catch (SQLException e) {
            Logger.logger.severe(e.getMessage());
        }
    }

    private static String result;

    public boolean findUser(String login, String password){

        try {
            ResultSet resultSet = statement.executeQuery("select PASSWORD from authorization where LOGIN = '" + login + "'");
            while (resultSet.next()) {
                result = resultSet.getString(1);
            }

        } catch (SQLException e) {
            Logger.logger.severe(e.getMessage());
        }
        return result.equals(password);
    }

    private ClientAccount newClient = new ClientAccount();

    public ClientAccount userInfo(String login){
        try {
            ResultSet resultSet = statement.executeQuery("select LOGIN, NAME, SURNAME, RATING, DISCOUNT from client where LOGIN = '" + login + "'");
            while (resultSet.next()) {
                newClient.setLogin(resultSet.getString("LOGIN"));
                newClient.setName(resultSet.getString("NAME"));
                newClient.setSurname(resultSet.getString("SURNAME"));
                newClient.setRating(resultSet.getFloat("RATING"));
                newClient.setDiscount(resultSet.getInt("DISCOUNT"));
            }

        } catch (SQLException e) {
            Logger.logger.severe(e.getMessage());
        }
        return newClient;
    }

    private List<Picture> catalogList = new LinkedList<>();

    public List<Picture> catalogTable(){
        try {
            ResultSet resultSet = statement.executeQuery("select PICTURE_ID, LINK, DESCRIPTION, RATING, AUTHOR from catalog");
            while (resultSet.next()) {
                catalogList.add(new Picture(resultSet.getInt("PICTURE_ID"),
                        resultSet.getString("LINK"),
                        resultSet.getString("DESCRIPTION"),
                        resultSet.getFloat("RATING"),
                        resultSet.getString("AUTHOR")));
            }

        } catch (SQLException e) {
            Logger.logger.severe(e.getMessage());
        }
        return catalogList;
    }

    private List<ClientAccount> clientList = new LinkedList<>();

    public List<ClientAccount> clientTable(){
        try {
            ResultSet resultSet = statement.executeQuery("select CLIENT_ID, LOGIN, NAME, SURNAME, RATING, DISCOUNT from client");
            while (resultSet.next()) {
                clientList.add(new ClientAccount(resultSet.getInt("CLIENT_ID"),
                        resultSet.getString("LOGIN"),
                        resultSet.getString("NAME"),
                        resultSet.getString("SURNAME"),
                        resultSet.getFloat("RATING"),
                        resultSet.getInt("DISCOUNT")));
            }
        } catch (SQLException e) {
            Logger.logger.severe(e.getMessage());
        }
        return clientList;
    }

    public void clientAdd(String login, String password, String name, String surname){
        try {
            statement.executeUpdate("insert into client (LOGIN, NAME, SURNAME) values ('" + login + "', '" + name + "', '" + surname + "')");
            statement.executeUpdate("insert into authorization (LOGIN, PASSWORD) values ('" + login + "', '" +  password + "')");
        } catch (SQLException e) {
            Logger.logger.severe(e.getMessage());
        }
    }

    private Float rating;

    public Float pictureRating(Integer id){
        try {
            ResultSet resultSet = statement.executeQuery("select RATING from catalog where PICTURE_ID = '" + id + "'");
            while (resultSet.next()){
                rating = resultSet.getFloat(1);
            }
        } catch (SQLException e) {
            Logger.logger.severe(e.getMessage());
        }
        return rating;
    }

    public void ratingUpdate(Integer pictureID, Float value){
        try {
            statement.executeUpdate("update catalog set RATING = '" + value + "' where PICTURE_ID = '" + pictureID + "'");
        } catch (SQLException e) {
            Logger.logger.severe(e.getMessage());
        }
    }

    public void userRatingUpdate(String login, Float value1, Integer value2){
        try {
            statement.executeUpdate("update client set RATING = '" + value1 + "'where LOGIN = '" + login + "'");
            statement.executeUpdate("update client set DISCOUNT = '" + value2 + "'where LOGIN = '" + login + "'");
        } catch (SQLException e) {
            Logger.logger.severe(e.getMessage());
        }
    }
}
