package by.epam.pahoda.utils;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

@WebServlet("/lang")
public class Language extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException{
        if(req.getParameter("ru") != null){
            Locale locale = new Locale("ru", "RU");
            ResourceBundle bundle = ResourceBundle.getBundle("text_ru_RU", locale);
            HttpSession session = req.getSession();
            session.setAttribute("bundle", bundle.getBaseBundleName());
            resp.sendRedirect("/Salon_war_exploded/index");
        }
        if(req.getParameter("be") != null){
            Locale locale = new Locale("be", "BY");
            ResourceBundle bundle = ResourceBundle.getBundle("text_be_BY", locale);
            HttpSession session = req.getSession();
            session.setAttribute("bundle", bundle.getBaseBundleName());
            resp.sendRedirect("/Salon_war_exploded/index");
        }
        if(req.getParameter("en") != null){
            Locale locale = new Locale("en", "US");
            ResourceBundle bundle = ResourceBundle.getBundle("text_en_US", locale);
            HttpSession session = req.getSession();
            session.setAttribute("bundle", bundle.getBaseBundleName());
            resp.sendRedirect("/Salon_war_exploded/index");
        }
    }
}
