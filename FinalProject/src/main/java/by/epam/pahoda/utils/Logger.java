package by.epam.pahoda.utils;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Handler;

public class Logger {

    public static java.util.logging.Logger logger = java.util.logging.Logger.getLogger(Logger.class.getName());

    static {
        try {
            Handler handler = new FileHandler("src/java0.log");
            logger.addHandler(handler);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
