package by.epam.pahoda.utils;

import by.epam.pahoda.entities.Picture;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class OrderPicture {

    public OrderPicture() {}

    static {
        Database.initClients();
    }

    private Picture picture;

    public Picture test(String link){
        try {
            ResultSet resultSet = Database.statement.executeQuery("select PICTURE_ID, LINK, DESCRIPTION, RATING, AUTHOR from catalog where LINK = '" + link + "'");
            while (resultSet.next()) {
                picture = new Picture(resultSet.getInt("PICTURE_ID"),
                        resultSet.getString("LINK"),
                        resultSet.getString("DESCRIPTION"),
                        resultSet.getFloat("RATING"),
                        resultSet.getString("AUTHOR"));
            }
        } catch (SQLException e) {
            Logger.logger.severe(e.getMessage());
        }
        return picture;
    }

    public void pictureOrder(String link, String description, String author){
        try {
            Database.statement.executeUpdate("insert into `orders` (link, description, author) values ('" + link + "', '" + description + "', '" + author + "')");
        } catch (SQLException e) {
            Logger.logger.severe(e.getMessage());
        }
    }

    private List<Picture> orderList = new LinkedList<>();

    public List<Picture> ordersTable(){
        try {
            ResultSet resultSet = Database.statement.executeQuery("select ID, LINK, DESCRIPTION, AUTHOR from `orders`");
            while (resultSet.next()) {
                orderList.add(new Picture(resultSet.getInt("ID"),
                        resultSet.getString("LINK"),
                        resultSet.getString("DESCRIPTION"),
                        resultSet.getString("AUTHOR")));
            }

        } catch (SQLException e) {
            Logger.logger.severe(e.getMessage());
        }
        return orderList;
    }

    private static String link, description, author;

    public void picAdd(Integer id){
        try {
            ResultSet resultSet = Database.statement.executeQuery("select link, description, author from orders where ID = '" + id + "'");
            while (resultSet.next()) {
                link = resultSet.getString("LINK");
                description = resultSet.getString("DESCRIPTION");
                author = resultSet.getString("AUTHOR");
            }
            Database.statement.executeUpdate("insert into catalog (LINK, DESCRIPTION, AUTHOR) values ('" + link + "', '" + description + "', '" + author + "')");
            Database.statement.executeUpdate("delete from orders where ID = '" + id + "'");
        } catch (SQLException e) {
            Logger.logger.severe(e.getMessage());
        }
    }

    public void picRemove(Integer id){
        try {
            Database.statement.executeUpdate("delete from orders where ID = '" + id + "'");
        } catch (SQLException e) {
            Logger.logger.severe(e.getMessage());
        }
    }
}
