package by.epam.pahoda.utils;

import by.epam.pahoda.entities.Picture;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class Searching {

    public Searching() {}

    static {
        Database.initClients();
    }

    private List<Picture> found = new LinkedList<>();

    public List<Picture> search(String str){
        try {
            ResultSet resultSet = Database.statement.executeQuery("select PICTURE_ID, LINK, DESCRIPTION, RATING, AUTHOR from catalog where CONCAT(PICTURE_ID, DESCRIPTION, RATING) LIKE '%" + str + "%'");
            while (resultSet.next()) {
                found.add(new Picture(resultSet.getInt("PICTURE_ID"),
                        resultSet.getString("LINK"),
                        resultSet.getString("DESCRIPTION"),
                        resultSet.getFloat("RATING"),
                        resultSet.getString("AUTHOR")));
            }
        } catch (SQLException e) {
            Logger.logger.severe(e.getMessage());
        }
        return found;
    }
}
