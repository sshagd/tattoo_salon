package by.epam.pahoda.servlets.catalog;

import by.epam.pahoda.entities.Picture;
import by.epam.pahoda.utils.Database;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

class CatalogServletTest extends Mockito {

    @Test
    void doGet() throws ServletException, IOException {
        final HttpServletRequest request = mock(HttpServletRequest.class);
        final HttpServletResponse response = mock(HttpServletResponse.class);
        final RequestDispatcher dispatcher = mock(RequestDispatcher.class);
        final Database database = mock(Database.class);

        List<Picture> pictures = database.catalogTable();

        when(database.catalogTable()).thenReturn(pictures);
        when(request.getRequestDispatcher("/pages/catalog-page.jsp")).thenReturn(dispatcher);

        new CatalogServlet().doGet(request, response);
        verify(database, times(1)).catalogTable();
        verify(dispatcher, times(1)).forward(request, response);

    }
}