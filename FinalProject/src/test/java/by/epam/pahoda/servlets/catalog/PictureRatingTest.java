package by.epam.pahoda.servlets.catalog;

import by.epam.pahoda.entities.ClientAccount;
import by.epam.pahoda.utils.Database;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

class PictureRatingTest extends Mockito {
    private static final Integer value = 14;
    private static final Float rating = 4f;

    @Test
    void doGet() throws IOException {
        final HttpServletRequest request = mock(HttpServletRequest.class);
        final HttpServletResponse response = mock(HttpServletResponse.class);
        final HttpSession session = mock(HttpSession.class);
        final ClientAccount client = mock(ClientAccount.class);
        final Database database = mock(Database.class);

        when(request.getParameter("rating")).thenReturn(String.valueOf(value));
        Integer pictureID = value / 10;
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("user")).thenReturn(client);
        when(database.userInfo(client.getLogin())).thenReturn(client);

        new PictureRating().doGet(request, response);
        Float newRating = 4f;
        verify(request, times(1)).getParameter("rating");
        verify(response, atMost(1)).sendRedirect("/Tariffs_war_exploded/catalog");
        verify(request, times(1)).getSession();
        verify(session, times(1)).getAttribute("user");
        verify(database, atMost(1)).userRatingUpdate("someLogin",
                (float)(client.getRating()+0.5), client.getDiscount() + 2);
        verify(database, atMost(1)).userInfo(client.getLogin());
        verify(session, atMost(1)).setAttribute("user", client);
    }

}