package by.epam.pahoda.servlets.catalog;

import by.epam.pahoda.entities.Picture;
import by.epam.pahoda.utils.Searching;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

class SearchingServletTest extends Mockito {
    private static final String str = "some string";

    @Test
    void doPost() throws ServletException, IOException {
        final HttpServletRequest request = mock(HttpServletRequest.class);
        final HttpServletResponse response = mock(HttpServletResponse.class);
        final RequestDispatcher dispatcher = mock(RequestDispatcher.class);
        final Searching searching = mock(Searching.class);

        List<Picture> pictures = searching.search(str);

        when(request.getParameter("param")).thenReturn(str);
        when(searching.search(str)).thenReturn(pictures);
        when(request.getRequestDispatcher("/pages/catalog-page.jsp")).thenReturn(dispatcher);

        new SearchingServlet().doPost(request, response);
        verify(searching, times(1)).search(str);
        verify(request, times(1)).setAttribute("pictures", pictures);
        verify(dispatcher, times(1)).forward(request, response);
    }
}