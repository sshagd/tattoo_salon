package by.epam.pahoda.servlets.order;

import by.epam.pahoda.entities.Picture;
import by.epam.pahoda.utils.OrderPicture;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

class OrderServletTest extends Mockito {

    @Test
    void doGet() throws ServletException, IOException {
        final HttpServletRequest request = mock(HttpServletRequest.class);
        final HttpServletResponse response = mock(HttpServletResponse.class);
        final RequestDispatcher dispatcher = mock(RequestDispatcher.class);
        final OrderPicture orderPicture = mock(OrderPicture.class);

        List<Picture> orders = orderPicture.ordersTable();

        when(orderPicture.ordersTable()).thenReturn(orders);
        when(request.getRequestDispatcher("/pages/orders.jsp")).thenReturn(dispatcher);

        new OrderServlet().doGet(request, response);
        verify(orderPicture, times(1)).ordersTable();
        verify(dispatcher, times(1)).forward(request, response);
    }
}