package by.epam.pahoda.servlets.redirect;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

class DBErrorTest extends Mockito {
    @Test
    void doGet() throws ServletException, IOException {
        doPost();
    }

    @Test
    void doPost() throws ServletException, IOException {
        final HttpServletRequest request = mock(HttpServletRequest.class);
        final HttpServletResponse response = mock(HttpServletResponse.class);
        final RequestDispatcher dispatcher = mock(RequestDispatcher.class);

        when(request.getRequestDispatcher("/pages/error_pages/DBError.jsp")).thenReturn(dispatcher);

        new DBError().doPost(request, response);
        verify(dispatcher, times(1)).forward(request, response);
    }
}