package by.epam.pahoda.servlets.user_action;

import by.epam.pahoda.entities.ClientAccount;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

class HomeServletTest extends Mockito {

    private static final String path1 = "/pages/user.jsp";
    private static final String path2 = "/index.jsp";

    @Test
    void doGet() throws ServletException, IOException {
        final HttpServletRequest request = mock(HttpServletRequest.class);
        final HttpServletResponse response = mock(HttpServletResponse.class);
        final HttpSession session = mock(HttpSession.class);
        final RequestDispatcher dispatcher = mock(RequestDispatcher.class);
        final ClientAccount client = mock(ClientAccount.class);

        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("user")).thenReturn(client);
        when(session.getAttribute("admin")).thenReturn("admin");
        when(request.getRequestDispatcher(path1)).thenReturn(dispatcher);
        when(request.getRequestDispatcher(path2)).thenReturn(dispatcher);

        new HomeServlet().doGet(request, response);
        verify(request, times(1)).getSession();
        verify(session, times(1)).getAttribute("user");
        verify(session, atMost(1)).getAttribute("admin");

        verify(dispatcher, atMost(1)).forward(request, response);
        verify(response, atMost(1)).sendRedirect("/Tariffs_war_exploded/user");
    }

}