package by.epam.pahoda.servlets.user_action;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

class LogOutTest extends Mockito {

    private static final String path = "/index.jsp";

    @Test
    void doGet() throws ServletException, IOException {
        final HttpServletRequest request = mock(HttpServletRequest.class);
        final HttpServletResponse response = mock(HttpServletResponse.class);
        final HttpSession session = mock(HttpSession.class);
        final RequestDispatcher dispatcher = mock(RequestDispatcher.class);

        when(request.getSession()).thenReturn(session);
        when(request.getRequestDispatcher(path)).thenReturn(dispatcher);

        new LogOut().doGet(request, response);
        verify(request, times(1)).getSession();
        verify(session, atMost(1)).removeAttribute("admin");
        verify(session, atMost(1)).removeAttribute("user");

        verify(dispatcher, times(1)).forward(request, response);

    }
}