package by.epam.pahoda.servlets.user_action;

import by.epam.pahoda.entities.ClientAccount;
import by.epam.pahoda.utils.Database;
import org.apache.commons.codec.digest.DigestUtils;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

class UserLogInTest extends Mockito {

    private static final String path1 = "/pages/adminProfile.jsp";
    private static final String path2 = "/pages/user.jsp";
    private static final String path3 = "/pages/error_pages/auth-error.jsp";

    @Test
    void doGet() throws ServletException, IOException {
        final HttpServletRequest request = mock(HttpServletRequest.class);
        final HttpServletResponse response = mock(HttpServletResponse.class);
        final HttpSession session = mock(HttpSession.class);
        final RequestDispatcher dispatcher = mock(RequestDispatcher.class);
        final ClientAccount client = mock(ClientAccount.class);

        List<ClientAccount> attrib = new UserLogIn().action();

        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("user")).thenReturn(client);
        when(session.getAttribute("admin")).thenReturn("admin");
        when(request.getRequestDispatcher(path1)).thenReturn(dispatcher);
        when(request.getRequestDispatcher(path2)).thenReturn(dispatcher);

        new UserLogIn().doGet(request, response);
        verify(request, times(1)).getSession();
        verify(session, atMost(1)).getAttribute("admin");
        verify(request, atMost(1)).setAttribute("clients", attrib);
        verify(dispatcher, atMost(1)).forward(request, response);
    }

    private static final String login = "primisen";
    private static final String password = "primi";

    @Test
    void doPost() throws ServletException, IOException {
        final HttpServletRequest request = mock(HttpServletRequest.class);
        final HttpServletResponse response = mock(HttpServletResponse.class);
        final HttpSession session = mock(HttpSession.class);
        final RequestDispatcher dispatcher = mock(RequestDispatcher.class);

        List<ClientAccount> attrib = new UserLogIn().action();

        when(request.getSession()).thenReturn(session);
        when(request.getParameter("login")).thenReturn(login);
        when(request.getParameter("password")).thenReturn(password);
        String passwordHash = DigestUtils.sha256Hex(password);
        ClientAccount client = new ClientAccount(login, passwordHash);
        ClientAccount currentClient = new Database().userInfo(login);

        when(session.getAttribute("user")).thenReturn(client);
        when(session.getAttribute("admin")).thenReturn("admin");
        when(request.getRequestDispatcher(path1)).thenReturn(dispatcher);
        when(request.getRequestDispatcher(path2)).thenReturn(dispatcher);
        when(request.getRequestDispatcher(path3)).thenReturn(dispatcher);

        new UserLogIn().doPost(request, response);
        verify(request, times(1)).getSession();
        verify(session, atMost(1)).setAttribute("admin", "admin");
        verify(request, atMost(1)).setAttribute("clients", attrib);
        verify(session, atMost(1)).setAttribute("user", currentClient);
        verify(dispatcher, atMost(1)).forward(request, response);
    }
}