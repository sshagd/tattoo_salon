package by.epam.pahoda.servlets.user_action;

import by.epam.pahoda.utils.Database;
import org.apache.commons.codec.digest.DigestUtils;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

class UserRegistrationTest extends Mockito {

    private static final String path1 = "/pages/error_pages/regError.jsp";
    private static final String path2 = "/pages/user.jsp";
    private static final String login = "primisen";
    private static final String password = "primi";
    private static final String name = "Nadzezhda";
    private static final String surname = "Shokel";

    @Test
    void doPost() throws ServletException, IOException {
        final HttpServletRequest request = mock(HttpServletRequest.class);
        final HttpServletResponse response = mock(HttpServletResponse.class);
        final HttpSession session = mock(HttpSession.class);
        final RequestDispatcher dispatcher = mock(RequestDispatcher.class);
        final Database db = mock(Database.class);

        when(request.getSession()).thenReturn(session);
        when(request.getParameter("login")).thenReturn(login);
        when(request.getParameter("pass")).thenReturn(password);
        String passwordHash = DigestUtils.sha256Hex(password);
        when(request.getParameter("name")).thenReturn(name);
        when(request.getParameter("surname")).thenReturn(surname);
        when(request.getRequestDispatcher(path1)).thenReturn(dispatcher);
        when(request.getRequestDispatcher(path2)).thenReturn(dispatcher);

        new UserRegistration().doPost(request, response);
        verify(request, times(1)).getParameter("login");
        verify(request, times(1)).getParameter("pass");
        verify(request, times(1)).getParameter("name");
        verify(request, times(1)).getParameter("surname");
        verify(db, atMost(1)).clientAdd(login, passwordHash, name, surname);
        verify(dispatcher, atMost(1)).forward(request, response);
    }
}