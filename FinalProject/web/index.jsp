<%@ page contentType="text/html;
charset=ISO-8859-5" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:requestEncoding value="UTF-8"/>
<%--@elvariable id="bundle" type="java.util.ResourceBundle"--%>
<c:set var="bundle" value="${bundle}" scope="session"/>
<fmt:setBundle basename="${bundle}" scope="session"/>

<html>
<head>
  <title>Best tattoo</title>
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>
<body>
<header class="w3-container w3-blue-grey w3-opacity w3-center" style="height: 70px; align-content: center">
    <h1 style="display: inline-block; padding: 10px; margin: inherit">Best tattoo!</h1>
    <form action="lang" method="post" style="display: inline-block">
    <button type="submit" name="ru" class="w3-btn w3-wheat w3-round-large w3-margin-bottom" style="display: inline-block; padding: 5px;">RU</button>
    <button type="submit" name="be" class="w3-btn w3-wheat w3-round-large w3-margin-bottom" style="display: inline-block; padding: 5px;">BE</button>
    <button type="submit" name="en" class="w3-btn w3-wheat w3-round-large w3-margin-bottom" style="display: inline-block; padding: 5px;">EN</button>
    </form>
</header>

  <h2 style="text-align: center;"><fmt:message key="message.welcome"/></h2>

<div class="w3-selection w3-light-grey w3-padding">
<form action="user" method="post" style="align-content: center">
  <label><fmt:message key="label.field1"/>
    <input type="text" name="login" class="w3-input w3-border w3-round-large" style="width: 20%" required><br />
  </label>
  <label><fmt:message key="label.field2"/>
    <input type="password" name="password" class="w3-input w3-border w3-round-large" style="width: 20%" required><br />
  </label>
  <button type="submit" name="enter" class="w3-btn w3-indigo w3-round-large w3-margin-bottom"><fmt:message key="label.button1"/></button>
</form>
<form action="redir" method="post">
  <button type="submit" name="registr" class="w3-btn w3-deep-purple w3-round-large w3-margin-bottom"><fmt:message key="label.button2"/></button>
</form>
</div>

</body></html>