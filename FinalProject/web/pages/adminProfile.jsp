<%--
  Created by IntelliJ IDEA.
  User: Sasha
  Date: 28.02.2019
  Time: 15:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;
charset=ISO-8859-5" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:requestEncoding value="UTF-8"/>
<%--@elvariable id="bundle" type="java.util.ResourceBundle"--%>
<c:set var="bundle" value="${sessionScope.bundle}" scope="session"/>
<fmt:setBundle basename="${bundle}" scope="session"/>
<html>
<head>
    <title><fmt:message key="message.admin"/></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>
<body>
<header class="w3-container w3-blue-grey w3-opacity w3-center" style="height: 70px; align-content: center">
    <label style="display: inline-block; padding: 15px;"><font size="5">Best tattoo!</font></label>
    <a href="catalog" style="display: inline-block; padding: 15px; font-size: large"><fmt:message key="header.label1"/></a>
    <label style="display: inline-block; padding: 15px;"><font size="5"><fmt:message key="header.label2"/></font></label>
    <a href="orders" style="display: inline-block; padding: 15px; font-size: large"><fmt:message key="header.label3"/></a>
    <a href="logout" style="display: inline-block; padding: 15px; font-size: large"><fmt:message key="header.label4"/></a>
    <form action="lang" method="post" style="display: inline-block">
        <button type="submit" name="ru" class="w3-btn w3-wheat w3-round-large w3-margin-bottom">RU</button>
        <button type="submit" name="be" class="w3-btn w3-wheat w3-round-large w3-margin-bottom">BE</button>
        <button type="submit" name="en" class="w3-btn w3-wheat w3-round-large w3-margin-bottom">EN</button>
    </form>
</header>

<h2 style="text-align: center;"><font size="5"><fmt:message key="message.admin"/></font></h2>

<table style="margin: auto">
    <tr>
        <td style="width: 30px"><fmt:message key="table.title1"/></td>
        <td style="width: 150px;"><fmt:message key="table.title2"/></td>
        <td style="width: 150px"><fmt:message key="table.title3"/></td>
        <td style="width: 150px"><fmt:message key="table.title4"/></td>
        <td style="width: 100px"><fmt:message key="table.title5"/></td>
        <td style="width: 100px"><fmt:message key="table.title6"/></td>
    </tr>
    <%--@elvariable id="clients" type="java.util.List"--%>
    <c:forEach var="clients" items="${clients}">
        <tr>
            <td>${clients.id}</td>
            <td>${clients.login}</td>
            <td>${clients.name}</td>
            <td>${clients.surname}</td>
            <td>${clients.rating}</td>
            <td>${clients.discount}</td>
        </tr>
    </c:forEach>
</table>

</body></html>