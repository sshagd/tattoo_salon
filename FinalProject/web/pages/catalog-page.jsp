<%--
  Created by IntelliJ IDEA.
  User: Sasha
  Date: 05.03.2019
  Time: 23:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:requestEncoding value="UTF-8"/>
<%--@elvariable id="bundle" type="java.util.ResourceBundle"--%>
<c:set var="bundle" value="${sessionScope.bundle}" scope="session"/>
<fmt:setBundle basename="${bundle}" scope="session"/>
<html>
<head>
    <title><fmt:message key="message.title2"/></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>
<body>
<header class="w3-container w3-blue-grey w3-opacity w3-center" style="height: 70px; align-content: center">
    <label style="display: inline-block; padding: 15px;"><font size="5">Best tattoo!</font></label>
    <label style="display: inline-block; padding: 15px;"><font size="5"><fmt:message key="message.title2"/></font></label>
    <a href="user" style="display: inline-block; padding: 15px; font-size: large"><fmt:message key="header.label5"/></a>
    <a href="logout" style="display: inline-block; padding: 15px; font-size: large"><fmt:message key="header.label4"/></a>
    <form action="lang" method="post" style="display: inline-block">
        <button type="submit" name="ru" class="w3-btn w3-wheat w3-round-large w3-margin-bottom">RU</button>
        <button type="submit" name="be" class="w3-btn w3-wheat w3-round-large w3-margin-bottom">BE</button>
        <button type="submit" name="en" class="w3-btn w3-wheat w3-round-large w3-margin-bottom">EN</button>
    </form>
</header>

<h2 style="text-align: center;"><font size="5"><fmt:message key="message.catalog"/></font></h2>

<form action="search" method="post" style="align-content: center">
    <label style="alignment: center"><fmt:message key="catalog.label1"/>
        <input type="text" name="param" class="w3-input w3-border w3-round-large" style="width: 30%">
    </label>
    <button type="submit" name="search" class="w3-btn w3-deep-purple w3-round-large w3-margin-bottom" style="display: inline-block"><fmt:message key="catalog.button1"/></button>
</form>

<form action="add" method="post">
    <button type="submit" name="add" class="w3-btn w3-deep-purple w3-round-large w3-margin-bottom"><fmt:message key="catalog.button2"/></button>
</form>

<table style="border: 1px solid black; width: 100%">
    <tr>
        <td><fmt:message key="table.title1"/></td>
        <td><fmt:message key="table.title7"/></td>
        <td><fmt:message key="table.title8"/></td>
        <td><fmt:message key="table.title5"/></td>
        <td><fmt:message key="table.title9"/></td>
        <td><fmt:message key="table.title10"/></td>
    </tr>
        <%--@elvariable id="pictures" type="java.util.List"--%>
        <c:forEach var="pictures" items="${pictures}">
    <tr>
        <td>${pictures.id}</td>
        <td><img src="${pictures.link}" style="width: 400px" alt="${pictures.description}"></td>
        <td>${pictures.description}</td>
        <td>${pictures.rating}</td>
        <td>
            <form action="pic-rate" method="post">
                <input type="radio" id="radiobutton5" name="rating" value="${pictures.id}5">
                <label for="radiobutton5">5</label>
                <input type="radio" id="radiobutton4" name="rating" value="${pictures.id}4">
                <label for="radiobutton4">4</label>
                <input type="radio" id="radiobutton3" name="rating" value="${pictures.id}3">
                <label for="radiobutton3">3</label>
                <input type="radio" id="radiobutton2" name="rating" value="${pictures.id}2">
                <label for="radiobutton2">2</label>
                <input type="radio" id="radiobutton1" name="rating" value="${pictures.id}1">
                <label for="radiobutton1">1</label>
                <button type="submit" style="display: inherit;"><fmt:message key="table.button3"/></button>
            </form>
        </td>
        <td>${pictures.author}</td>
        </c:forEach>
    </tr>
</table>

</body>
</html>
