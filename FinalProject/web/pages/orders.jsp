<%--
  Created by IntelliJ IDEA.
  User: Sasha
  Date: 05.03.2019
  Time: 23:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:requestEncoding value="UTF-8"/>
<%--@elvariable id="bundle" type="java.util.ResourceBundle"--%>
<c:set var="bundle" value="${sessionScope.bundle}" scope="session"/>
<fmt:setBundle basename="${bundle}" scope="session"/>
<html>
<head>
    <title><fmt:message key="message.title3"/></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>
<body>
<header class="w3-container w3-blue-grey w3-opacity w3-center" style="height: 70px; align-content: center">
    <label style="display: inline-block; padding: 15px;"><font size="5">Best tattoo!</font></label>
    <label style="display: inline-block; padding: 15px;"><font size="5"><fmt:message key="message.title3"/></font></label>
    <a href="user" style="display: inline-block; padding: 15px; font-size: large"><fmt:message key="header.label5"/></a>
    <a href="logout" style="display: inline-block; padding: 15px; font-size: large"><fmt:message key="header.label4"/></a>
    <form action="lang" method="post" style="display: inline-block">
        <button type="submit" name="ru" class="w3-btn w3-wheat w3-round-large w3-margin-bottom" style="display: inline-block; padding: 5px;">RU</button>
        <button type="submit" name="be" class="w3-btn w3-wheat w3-round-large w3-margin-bottom" style="display: inline-block; padding: 5px;">BE</button>
        <button type="submit" name="en" class="w3-btn w3-wheat w3-round-large w3-margin-bottom" style="display: inline-block; padding: 5px;">EN</button>
    </form>
</header>

<h2 style="text-align: center;"><font size="5"><fmt:message key="message.orders"/></font></h2>

<table style="border: 1px solid black; margin: auto">
    <tr>
        <td><fmt:message key="table.title1"/></td>
        <td><fmt:message key="table.title7"/></td>
        <td><fmt:message key="table.title8"/></td>
        <td><fmt:message key="table.title10"/></td>
        <td><fmt:message key="table.title11"/></td>
    </tr>
    <%--@elvariable id="orders" type="java.util.List"--%>
    <c:forEach var="orders" items="${orders}">
    <tr>
        <td>${orders.id}</td>
        <td><img src="${orders.link}" style="width: 300px" alt="${orders.description}"></td>
        <td>${orders.description}</td>
        <td>${orders.author}</td>
        <td>
            <form action="response" method="post">
                <button type="submit" name="yes" value="${orders.id}"><fmt:message key="orders.button1"/></button>
                <button type="submit" name="no" value="${orders.id}"><fmt:message key="orders.button2"/></button>
            </form>
        </td>
        </c:forEach>
    </tr>
</table>

</body>
</html>
