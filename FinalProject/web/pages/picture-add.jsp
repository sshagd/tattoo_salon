<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:requestEncoding value="UTF-8"/>
<%--@elvariable id="bundle" type="java.util.ResourceBundle"--%>
<c:set var="bundle" value="${sessionScope.bundle}" scope="session"/>
<fmt:setBundle basename="${bundle}" scope="session"/>
<html>
<head>
    <title><fmt:message key="message.title4"/></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>
<body>
<div class="w3-container w3-blue-grey w3-opacity w3-left-align">
    <label style="display: inline-block; padding: 15px;"><font size="5">Best tattoo!</font></label>
</div>
<h2 style="text-align: center;"><font size="5"><fmt:message key="message.title4"/></font></h2>

<form action="pic-add" method="post" class="w3-selection w3-light-grey w3-padding">
    <label><fmt:message key="adding.label1"/>
        <input type="text" name="link" class="w3-input w3-border w3-round-large" style="width: 30%" required><br />
    </label>
    <label><fmt:message key="adding.label2"/>
        <input type="text" name="description" class="w3-input w3-border w3-round-large" style="width: 30%" required><br />
        <fmt:message key="adding.label3"/>
    </label>
    <br/>
    <button type="submit" class="w3-btn w3-indigo w3-round-large w3-margin-bottom"><fmt:message key="orders.button"/></button>
</form>
</body>
</html>
