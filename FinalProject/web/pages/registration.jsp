<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:requestEncoding value="UTF-8"/>
<%--@elvariable id="bundle" type="java.util.ResourceBundle"--%>
<c:set var="bundle" value="${sessionScope.bundle}" scope="session"/>
<fmt:setBundle basename="${bundle}" scope="session"/>
<html>
<head>
    <title><fmt:message key="message.title5"/></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>
<body>
<header class="w3-container w3-blue-grey w3-opacity w3-center" style="height: 70px; align-content: center">
    <label style="display: inline-block; padding: 15px;"><font size="5">Best tattoo!</font></label>
    <form action="lang" method="post" style="display: inline-block">
        <button type="submit" name="ru" class="w3-btn w3-wheat w3-round-large w3-margin-bottom">RU</button>
        <button type="submit" name="be" class="w3-btn w3-wheat w3-round-large w3-margin-bottom">BE</button>
        <button type="submit" name="en" class="w3-btn w3-wheat w3-round-large w3-margin-bottom">EN</button>
    </form>
</header>
<h2 style="text-align: center;"><font size="5"><fmt:message key="message.welcome"/></font></h2>

<form action="registr" method="post" class="w3-selection w3-light-grey w3-padding">
    <label><fmt:message key="label.field1"/>
        <input type="text" name="login" class="w3-input w3-border w3-round-large" style="width: 30%" required><br />
    </label>
    <label><fmt:message key="label.field2"/>
        <input type="password" name="pass" class="w3-input w3-border w3-round-large" style="width: 30%" required><br />
    </label>
    <label><fmt:message key="label.field3"/>
        <input type="text" name="name" class="w3-input w3-border w3-round-large" style="width: 30%" required><br />
    </label>
    <label><fmt:message key="label.field4"/>
        <input type="text" name="surname" class="w3-input w3-border w3-round-large" style="width: 30%" required><br />
    </label>
    <button type="submit" class="w3-btn w3-indigo w3-round-large w3-margin-bottom"><fmt:message key="registration.button"/></button>
</form>
</body>
</html>
