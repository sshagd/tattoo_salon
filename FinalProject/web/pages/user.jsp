<%--@elvariable id="user" type="by.epam.pahoda.entities.ClientAccount"--%>
<%@ page contentType="text/html;
charset=ISO-8859-5" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:requestEncoding value="UTF-8"/>
<%--@elvariable id="bundle" type="java.util.ResourceBundle"--%>
<c:set var="bundle" value="${sessionScope.bundle}" scope="session"/>
<fmt:setBundle basename="${bundle}" scope="session"/>
<html>
<head>
    <title><fmt:message key="message.title6"/></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>
<body>
<header class="w3-container w3-blue-grey w3-opacity w3-center" style="height: 70px; align-content: center">
    <h1 style="display: inline-block; padding: 10px; margin: inherit">Best tattoo!</h1>
    <a href="catalog" style="display: inline-block; padding: 15px; font-size: large"><fmt:message key="message.title2"/></a>
    <label style="display: inline-block; padding: 15px;"><font size="5"><fmt:message key="header.label5"/></font></label>
    <a href="logout" style="display: inline-block; padding: 15px; font-size: large"><fmt:message key="header.label4"/></a>
    <form action="lang" method="post" style="display: inline-block">
        <button type="submit" name="ru" class="w3-btn w3-wheat w3-round-large w3-margin-bottom" style="display: inline-block; padding: 5px;">RU</button>
        <button type="submit" name="be" class="w3-btn w3-wheat w3-round-large w3-margin-bottom" style="display: inline-block; padding: 5px;">BE</button>
        <button type="submit" name="en" class="w3-btn w3-wheat w3-round-large w3-margin-bottom" style="display: inline-block; padding: 5px;">EN</button>
    </form>
</header>

<h2 style="text-align: center;"><font size="5"><fmt:message key="message.user"/></font></h2>

<table style="border: 1px solid black; margin: auto">
    <tr>
        <td style="width: 150px"><fmt:message key="table.title2"/></td>
        <td style="width: 150px"><fmt:message key="table.title3"/></td>
        <td style="width: 150px"><fmt:message key="table.title4"/></td>
        <td style="width: 100px"><fmt:message key="table.title5"/></td>
        <td style="width: 100px"><fmt:message key="table.title6"/></td>
    </tr>
    <tr>
        <td>${user.login}</td>
        <td>${user.name}</td>
        <td>${user.surname}</td>
        <td>${user.rating}</td>
        <td>${user.discount}</td>
    </tr>
</table>

</body>
</html>
